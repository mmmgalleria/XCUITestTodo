//
//  UITestEnum.swift
//  TodoUITests
//
//  Created by JIRATH EAKTHITIWORRANUN on 28/9/2563 BE.
//  Copyright © 2563 YiGu. All rights reserved.
//

import XCTest

enum ToDoType: String {
    case child = "child"
    case phone = "phone"
    case shoppingCart = "shopping-cart"
    case travel = "travel"
}

enum MonthOfYear: String{
    case January
    case February
    case March
    case April
    case May
    case June
    case July
    case August
    case September
    case October
    case November
    case December
}

